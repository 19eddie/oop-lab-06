package it.unibo.oop.lab.collections1;

import java.util.*;

/**
 * Example class using {@link java.util.List} and {@link java.util.Map}.
 * 
 */
public final class UseCollection {
	
    private static final int TO_MS = 1_000_000;

    private UseCollection() {
    }

    /**
     * @param s
     *            unused
     */
    public static void main(final String... s) {
        /*
         * 1) Create a new ArrayList<Integer>, and populate it with the numbers
         * from 1000 (included) to 2000 (excluded).
         */
    	ArrayList<Integer> arrList = new ArrayList<>();
    	for(int i=1000; i<2000; i++) {
    		arrList.add(i);
    	}
        /*
         * 2) Create a new LinkedList<Integer> and, in a single line of code
         * without using any looping construct (for, while), populate it with
         * the same contents of the list of point 1.
         */
    	LinkedList<Integer> linkList = new LinkedList<>(arrList);
        /*
         * 3) Using "set" and "get" and "size" methods, swap the first and last
         * element of the first list. You can not use any "magic number".
         * (Suggestion: use a temporary variable)
         */
    	for(int i=0; i<arrList.size()/2; i++) {
    		int tmp=arrList.get(i);
    		arrList.set(i,arrList.get(arrList.size()-i-1));
    		arrList.set(arrList.size()-i-1, tmp);
    	}
        /*
         * 4) Using a single for-each, print the contents of the arraylist.
         */
    	for(int tmp : arrList) {
    		System.out.println(tmp);
    	}
        /*
         * 5) Measure the performance of inserting new elements in the head of
         * the collection: measure the time required to add 100.000 elements as
         * first element of the collection for both ArrayList and LinkedList,
         * using the previous lists. In order to measure times, use as example
         * TestPerformance.java.
         */
        long time = System.nanoTime();
        for(int i=0; i<100_000; i++) {
        	arrList.add(0, i);
        }
        time = System.nanoTime() - time;
        System.out.println("ArrayList: "+time/TO_MS);
        time = System.nanoTime();
        for(int i=0; i<100_000; i++) {
        	linkList.add(0, i);
        }
        time = System.nanoTime() - time;
        System.out.println("LinkedList: "+time/TO_MS);
        /*
         * 6) Measure the performance of reading 1000 times an element whose
         * position is in the middle of the collection for both ArrayList and
         * LinkedList, using the collections of point 5. In order to measure
         * times, use as example TestPerformance.java.
         */
        time = System.nanoTime();
        for(int i=0; i<1000; i++) {
        	arrList.get(arrList.size()/2);
        }
        time = System.nanoTime() - time;
        System.out.println("ArrayList: "+time/TO_MS);
        time = System.nanoTime();
        for(int i=0; i<1000; i++) {
        	linkList.get(linkList.size()/2);
        }
        time = System.nanoTime() - time;
        System.out.println("LinkedList: "+time/TO_MS);
        
        /*
         * 7) Build a new Map that associates to each continent's name its
         * population:
         * 
         * Africa -> 1,110,635,000
         * 
         * Americas -> 972,005,000
         * 
         * Antarctica -> 0
         * 
         * Asia -> 4,298,723,000
         * 
         * Europe -> 742,452,000
         * 
         * Oceania -> 38,304,000
         */
        Map<String, Long> continentMap = new HashMap<>();
        continentMap.put("Africa",  1_110_635_000L);
        continentMap.put("Americas",  972_005_000L);
        continentMap.put("Antarctica",  0L);
        continentMap.put("Asia",  4_298_723_000L);
        continentMap.put("Europe",  742_452_000L);
        continentMap.put("Oceania",  38_304_000L);
        /*
         * 8) Compute the population of the world
         */
        long totalPopulation=0L;
        for(long tmp : continentMap.values()) {
        	totalPopulation=totalPopulation+tmp;
        }
        System.out.println("Total population: "+ totalPopulation);
    }
}
